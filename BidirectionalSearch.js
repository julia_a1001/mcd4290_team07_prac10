/* Write your function for implementing the Bi-directional Search algorithm here */

/*
Average time - 6.74 ms

*/
function bidirectionalSearchTest(data)
{
	let counter = 0;
	let middle_of_array = Math.floor(data.length / 2);
	
		
	while (counter <  middle_of_array)
	{
		if (data[middle_of_array - counter].emergency === true)
		{
			return data[middle_of_array - counter].address;
		}
	
		else
		{
			if (data[middle_of_array + counter] === true)
			{
				return data[middle_of_array + counter].address;
			}
			else
			{
				counter += 1;
			}
		}
	}
	
}



/*	
	function step2(data){
		let obj_at_middle = middle_of_array - counter;
				
		for (obj_at_middle; obj_at_middle < data.length; obj_at_middle++)
		{
			if(data[obj_at_middle].emergency === true)
			{
		
				return data[obj_at_middle].address;
		
			}
		
			else
			{
				step3(data);
			}
			
			
		}
		
		
	}
	
	
	function step3(data)
	{
			
		let middle = middle_of_array + counter;
		
		for (middle; middle < data.length; middle++)
		{
				if(data[middle].emergency === true)
			{
				return data.address[middle];
			}
		
			else if(data[middle].emergency === false)
			{
				counter += 1;
			
					if(counter <= (data.length)/2)
					{
				
						step2(data);
					}
			
					else if(counter > (data.length)/2)
					{
						return null;
					}
			
			}
			
		}
		
		
	
	}
	*/